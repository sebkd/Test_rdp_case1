"""
Программа-калькулятор
дается формула типа
one(delit(two(minus(three(plus(five(umnojit(four))))))))
виду того, что программа не знает сколько операций может быть, вычисление производится
справо налево)))
4 * 5 + 3 - 2 / 1 = 21
для вывода в консоль используйте print
"""

check = []


def verify_len():
    """Функция проверки на 2 числа, чтобы выполнить операцию"""
    if len(check) < 3:
        return False
    else:
        return True


def calculate():
    """Функция вычисления между двумя числами"""
    value = 0
    if '*' in check:
        value = check[-3] * check[-1]
    elif '-' in check:
        value = check[-3] - check[-1]
    elif '+' in check:
        value = check[-3] + check[-1]
    else:
        value = check[-3] / check[-1]
    check.clear()
    check.insert(0, value)
    return check


def transfer():
    """Функция передачи вычисленного значения двух чисел"""
    if verify_len():
        return calculate()
    else:
        return check


def plus(obj=' '):
    """Функция +"""
    def wrapper():
        print('зашел wrapper +')
        obj()
        check.append('+')
    if callable(obj):
        return wrapper()
    else:
        check.append('+')


def minus(obj=' '):
    """Функция -"""
    def wrapper():
        obj()
        check.append('-')
    if callable(obj):
        return wrapper()
    else:
        check.append('-')


def umnojit(obj=' '):
    """Функция *"""
    def wrapper():
        obj()
        check.append('*')
    if callable(obj):
        return wrapper()
    else:
        check.append('*')


def delit(obj=' '):
    """Функция /"""
    def wrapper():
        obj()
        check.append('/')
    if callable(obj):
        return wrapper()
    else:
        check.append('/')


def one(obj=' '):
    """Функция 1"""
    check.append(1)
    return transfer()[-1]


def two(obj=' '):
    """Функция 2"""
    check.append(2)
    return transfer()[-1]


def three(obj=' '):
    """Функция 3"""
    check.append(3)
    return transfer()[-1]


def four(obj=' '):
    """Функция 4"""
    check.append(4)
    return transfer()[-1]


def five(obj=' '):
    """Функция 5"""
    check.append(5)
    return transfer()[-1]


if __name__ == '__main__':
    print(three(delit(two(minus(three(plus(five(umnojit(four)))))))))
